protectionmodule = {name: "protectionmodule"}

#region modulesFromEnvironment
fs = require "fs-extra"
#region localModules
cfg = null
globalScope = null
#endregion
#endregion

#region logPrintFunctions
##############################################################################
log = (arg) ->
    if allModules.debugmodule.modulesToDebug["protectionmodule"]?  then console.log "[protectionmodule]: " + arg
    return
olog = (o) -> log "\n" + ostr(o)
ostr = (o) -> JSON.stringify(o, null, 4)
#endregion
##############################################################################
protectionmodule.initialize = () ->
    log "protectionmodule.initialize"
    cfg = allModules.configmodule
    globalScope = allModules.globalscopemodule
    return

#region internalProperties
protectedThingies = {}
#endregion

#region internalFunctions
loadProtected = ->
    log "loadProtected"
    try protectedThingies = require cfg.cli.protectedThingiesPath
    catch err then protectedThingies = {}
    olog protectedThingies
    return

saveProtected = ->
    log "saveProtected"
    ## we deliberately donot await here
    fs.writeFile(cfg.cli.protectedThingiesPath, ostr(protectedThingies))
    return

#endregion

#region exposedFunctions
protectionmodule.protect = (thingies) ->
    log "protectionmodule.protect"
    protectedThingies[thingy] = true for thingy in thingies
    saveProtected()
    return

protectionmodule.stopProtecting = (thingies) ->
    log "protectionmodule.stopProtecting"
    delete protectedThingies[thingy] for thingy in thingies
    saveProtected()
    return

protectionmodule.getUnprotected = ->
    log "protectionmodule.getUnprotected"
    all = globalScope.getAllThingiesInScope()
    unprotected = (thingy for thingy in all when !protectedThingies[thingy])
    return unprotected

protectionmodule.getProtected = () ->
    log "protectionmodule.getProtected"
    return Object.keys(protectedThingies)

protectionmodule.load = loadProtected
#endregion

module.exports = protectionmodule