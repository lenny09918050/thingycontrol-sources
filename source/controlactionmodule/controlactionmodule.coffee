controlactionmodule = {name: "controlactionmodule"}

#region modulesFromEnvironment
control = null
#endregion

#region userActions
allUserActions =

    deleteSelection:
        userChoiceLabel: "delete selection"
        execute: ->
            log "execution of deleteSelectionAction"
            await control.deleteSelection()
            
    protectSelection: 
        userChoiceLabel: "protect selection" 
        execute: ->
            log " execution of protectSelectionAction"
            await control.protectSelection()

    stopProtectingSelection: 
        userChoiceLabel: "stop protecting selection"
        execute: ->
            log "execution of stopProtectingSelectionAction"
            await control.stopProtectingSelection()

    skip:
        userChoiceLabel: "skip"
        execute: -> throw false

allActions = Object.keys(allUserActions)
#endregion

#region logPrintFunctions
##############################################################################
log = (arg) ->
    if allModules.debugmodule.modulesToDebug["controlactionmodule"]?  then console.log "[controlactionmodule]: " + arg
    return
olog = (o) -> log "\n" + JSON.stringify(o, null, 4)
#endregion
##############################################################################
controlactionmodule.initialize = () ->
    log "controlactionmodule.initialize"
    control = allModules.controlmodule
    return

#region internalFunctions
getActionChoice = (action) ->
    log "getActionChoice"
    choice = 
        name: action.userChoiceLabel
        value: action
    return choice
#endregion

#region exposedFunctions
controlactionmodule.doAction = (action) ->
    log "controlactionmodule.doAction"
    await action.execute()

controlactionmodule.addActionChoices = (choices) ->
    log "controlactionmodule.addActionChoices"
    for action in allActions
        choices.push getActionChoice(allUserActions[action])
    return choices
#endregion

module.exports = controlactionmodule