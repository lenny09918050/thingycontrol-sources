controlmodule = {name: "controlmodule"}

#region modulesFromEnvironment
#region node_modules
fs = require "fs-extra"
CLI = require('clui')
Spinner = CLI.Spinner
chalk = require "chalk"
#endregion

#region localModules
user = null
cloud = null
protection = null
controlAction = null
#endregion
#endregion

#region logPrintFunctions
log = (arg) ->
    if allModules.debugmodule.modulesToDebug["controlmodule"]?  then console.log "[controlmodule]: " + arg
    return
olog = (o) -> log "\n" + ostr(o)
ostr = (o) -> JSON.stringify(o, null, 4)
printError = (arg) -> console.log(chalk.red(arg))
#endregion
##############################################################################
controlmodule.initialize = () ->
    log "controlmodule.initialize"
    user = allModules.userinquirermodule
    cloud = allModules.cloudservicemodule
    protection = allModules.protectionmodule
    controlAction = allModules.controlactionmodule
    return   

#region internalFunctions
controlActionDecision = ->
    log "controlActionDecision"
    actionChoices = []
    controlAction.addActionChoices(actionChoices)
    message = "Select a controlAction >"
    return await user.inquireNextAction(actionChoices, message)

deleteThingies = (thingies) ->
    log "deleteThingies"
    promises = (cloud.deleteRepository(thingy) for thingy in thingies)
    statusMessage = "killing the thingies with Fire!..."
    status = new Spinner(statusMessage)
    status.start()
    try await Promise.all(promises)
    catch err then log err
    finally status.stop()
    return

#endregion

#region exposed
controlmodule.doInterogation = ->
    log "controlmodule.doInterogation"
    loop
        action = await controlActionDecision()
        try await controlAction.doAction(action)
        catch err 
            return if !err
            log err

#region controlActionExecution
controlmodule.deleteSelection = ->
    log "controlmodule.deleteSelection"
    unprotected = protection.getUnprotected()
    message = "! Select the thingies to DELETE >"
    toDelete = await user.inquireSelectionSet(unprotected, message)
    await deleteThingies(toDelete)
    return

controlmodule.protectSelection = ->
    log "controlmodule.protectSelection"
    unprotected = protection.getUnprotected()
    message = "Select the thingies to protect >"
    toProtect = await user.inquireSelectionSet(unprotected, message)
    await protection.protect(toProtect)
    return

controlmodule.stopProtectingSelection = ->
    log "controlmodule.stopProtectingSelection"
    theProtected = protection.getProtected()
    message = "Select the thingies to stop protecting >"
    stopProtect = await user.inquireSelectionSet(theProtected, message)
    await protection.stopProtecting(stopProtect)
    return
#endregion
#endregion

module.exports = controlmodule