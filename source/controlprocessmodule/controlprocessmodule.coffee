controlprocessmodule = {name: "controlprocessmodule"}

#region modulesFromEnvironment
cfg = null
control = null
protection = null
#endregion

#region logPrintFunctions
##############################################################################
log = (arg) ->
    if allModules.debugmodule.modulesToDebug["controlprocessmodule"]?  then console.log "[controlprocessmodule]: " + arg
    return
olog = (o) -> log "\n" + ostr(o)
ostr = (o) -> JSON.stringify(o, null, 4)
print = (arg) -> console.log(arg)
#endregion
##############################################################################
controlprocessmodule.initialize = () ->
    log "controlprocessmodule.initialize"
    cfg = allModules.configmodule
    control = allModules.controlmodule
    protection = allModules.protectionmodule
    return 

#region internalFunctions
#endregion

#region exposedFunctions
controlprocessmodule.execute = (configure) ->
    log "controlprocessmodule.execute"
    await cfg.checkUserConfig(configure)
    await protection.load()
    await control.doInterogation()
    return
#endregion

module.exports = controlprocessmodule
