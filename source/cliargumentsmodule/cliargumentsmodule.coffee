cliargumentsmodule = {name: "cliargumentsmodule"}

#region node_modules
meow = require("meow")
#endregion

#log Switch
log = (arg) ->
    if allModules.debugmodule.modulesToDebug["cliargumentsmodule"]?  then console.log "[cliargumentsmodule]: " + arg
    return

##initialization function  -> is automatically being called!  ONLY RELY ON DOM AND VARIABLES!! NO PLUGINS NO OHTER INITIALIZATIONS!!
cliargumentsmodule.initialize = () ->
    log "cliargumentsmodule.initialize"

#region internal functions
getHelpText = ->
    log "getHelpText"
    return """
        Usage
            $ thingycontrol
    
        Options
            optional:
                --configure, -c
                    flag, if available then we start configuring our userConfig first

        Examples
            $ thingycontrol -c
            ...
    """

getOptions = ->
    log "getOptions"
    return {
        flags:
            configure: 
                type: "boolean"
                alias: "c"
    }

extractMeowed = (meowed) ->
    log "extractMeowed"
    configure = false

    if meowed.flags.configure then configure = true

    return {configure}

#endregion

#region exposed functions
cliargumentsmodule.extractArguments = ->
    log "cliargumentsmodule.extractArguments"
    options = getOptions()
    meowed = meow(getHelpText(), getOptions())
    extract = extractMeowed(meowed)
    return extract

#endregion exposed functions

module.exports = cliargumentsmodule