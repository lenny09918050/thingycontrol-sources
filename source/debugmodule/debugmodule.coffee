debugmodule = {name: "debugmodule"}

##############################################################################
debugmodule.initialize = () ->
    # console.log "debugmodule.initialize - nothing to do"
    return     
##############################################################################
debugmodule.modulesToDebug = 
    unbreaker: true
    # cliargumentsmodule: true
    # cloudservicemodule: true
    # configmodule: true
    # constructionmodule: true
    # controlmodule: true
    # controlprocessmodule: true
    # encryptionmodule: true
    # githubservicemodule: true
    # gitlabservicemodule: true
    # gitmodule: true
    # globalscopemodule: true
    # pathhandlermodule: true
    # protectionmodule: true
    # recipemodule: true
    # remotehandlermodule: true
    # startupmodule: true
    # urlhandlermodule: true
    # useractionmodule: true
    # userconfigmodule: true
    # userinquirermodule: true

module.exports = debugmodule