Modules =
    cliargumentsmodule: require "./cliargumentsmodule"
    cloudservicemodule: require "./cloudservicemodule"
    configmodule: require "./configmodule"
    controlactionmodule: require "./controlactionmodule"
    controlmodule: require "./controlmodule"
    controlprocessmodule: require "./controlprocessmodule"
    debugmodule: require "./debugmodule"
    encryptionmodule: require "./encryptionmodule"
    globalscopemodule: require "./globalscopemodule"
    pathhandlermodule: require "./pathhandlermodule"
    protectionmodule: require "./protectionmodule"
    startupmodule: require "./startupmodule"
    useractionmodule: require "./useractionmodule"
    userconfigmodule: require "./userconfigmodule"
    userinquirermodule: require "./userinquirermodule"

module.exports = Modules