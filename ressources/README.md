# thingycontrol - pest control for thingies

# Why?
Now it is easy to create thingies, tons of thingies, we may create the living thingy plague.

This is the cure.

# What?

thingycontrol is a small cli tool, which takes advantage of the way how cloudservices are handled by [thingycreate](https://www.npmjs.com/package/thingycreate). This access then is used to delete the unwanted thingies which otherwise easily grow out of control^^.

Also there is the option to store thingies you definately don't want to delete in a protectedThingies.json file. 

# How?
Requirements
------------
* [GitHub account](https://github.com/) and/or [Gitlab account](https://gitlab.com/)
* [GitHub ssh-key access](https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) and/or [Gitlab ssh-key-access](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html)
* [GitHub access token (repo scope)](https://github.com/settings/tokens) and/or [Gitlab access token (api scope)](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
* [Git installed](https://git-scm.com/)
* [Node.js installed](https://nodejs.org/)
* [Npm installed](https://www.npmjs.com/)
* [OpenSSH installed](https://www.openssh.com/)

Installation
------------

Npm Registry
``` sh
$ npm install -g thingycontrol
```
Current git version
``` sh
$ npm install -g git+https://gitlab.com/lenny09918050/thingycontrol-output.git
```

Usage
-----

```
  Usage
      $ thingycontrol
    
  Options
      optional:
          --configure, -c
              flag, if available then we start configuring our userConfig first

  Examples
      $ thingycontrol -c
      ...

```

Call the script as `thingycontrol` with `-c` as option to handle user configurations

The userConfig works in the same way as for thingycreate. Just the recipe functions have been cut off.

Then there are our main actions.

Main Actions
---

```sh
? Select an action > (Use arrow keys)
  ──────────────
❯ delete selection 
  protect selection 
  stop protecting selection 
  skip 
```

- delete selection: Here you have a choice of all unprotected thingies to delete.

- protect selection: Here you have a choice of all unprotected thingies to protect them.

    Protecting a thingy means that it is being added to the protectedThingies.json. This has the effect that it won't appear as an option to delete.

- stop protecting selection :Here you have a choice of all protected thingies to stop protecting them.

# Further steps

- clean out code a bit more
- integrate into thingycreate?
- ...

All sorts of inputs are welcome, thanks!

---

# License

## The Unlicense JhonnyJason style

- Information has no ownership.
- Information only has memory to reside in and relations to be meaningful.
- Information cannot be stolen. Only shared or destroyed.

And you wish it has been shared before it is destroyed.

The one claiming copyright or intellectual property either is really evil or probably has some insecurity issues which makes him blind to the fact that he also just connected information which was freely available to him.

The value is not in him who "created" the information the value is what is being done with the information.
So the restriction and friction of the informations' usage is exclusively reducing value overall.

The only preceived "value" gained due to restriction is actually very similar to the concept of blackmail (power gradient, control and dependency).

The real problems to solve are all in the "reward/credit" system and not the information distribution. Too much value is wasted because of not solving the right problem.

I can only contribute in that way - none of the information is "mine" everything I "learned" I actually also copied.
I only connect things to have something I feel is missing and share what I consider useful. So please use it without any second thought and please also share whatever could be useful for others. 

I also could give credits to all my sources - instead I use the freedom and moment of creativity which lives therein to declare my opinion on the situation. 

*Unity through Intelligence.*

We cannot subordinate us to the suboptimal dynamic we are spawned in, just because power is actually driving all things around us.
In the end a distributed network of intelligence where all information is transparently shared in the way that everyone has direct access to what he needs right now is more powerful than any brute power lever.

The same for our programs as for us.

It also is peaceful, helpful, friendly - decent. How it should be, because it's the most optimal solution for us human beings to learn, to connect to develop and evolve - not being excluded, let hanging and destroy oneself or others.

If we really manage to build an real AI which is far superior to us it will unify with this network of intelligence.
We never have to fear superior intelligence, because it's just the better engine connecting information to be most understandable/usable for the other part of the intelligence network.

The only thing to fear is a disconnected unit without a sufficient network of intelligence on its own, filled with fear, hate or hunger while being very powerful. That unit needs to learn and connect to develop and evolve then.

We can always just give information and hints :-) The unit needs to learn by and connect itself.

Have a nice day! :D